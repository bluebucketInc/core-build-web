
'use strict';

//Angular module
angular
	.module('uobApp')

	.controller('mainController', ['$scope', '$state', function($scope, $state) {

		// vars
		$scope.$state 		= $state;
		$scope.data			= [];
		$scope.data.date 	= new Date();
		$scope.monthNames 	= ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

		// AM / PM Date
		$scope.formatAMPM = function(date) {
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var ampm = hours >= 12 ? 'pm' : 'am';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			return strTime;
		};

	}])

	// PIN controller
	.controller('idleController', [ '$scope', '$state', function($scope, $state) {

		// on space bar hit
		$(document).keydown(function (e) {
			if (e.keyCode === 48 || e.keyCode === 96) {
				
				e.preventDefault()
				// get new time
				$scope.data.date = new Date();
				// startdate
				$scope.data.startDate 	= $scope.data.date.getDay() + " " + $scope.monthNames[$scope.data.date.getMonth()] + " " + $scope.data.date.getFullYear() + " " + $scope.formatAMPM($scope.data.date) + " SGT";
				// go to pin
				$state.go('pin');
			};
		});

	}])

	// PIN controller
	.controller('pinController', [ '$scope', '$state', '$timeout', function($scope, $state, $timeout) {

		// unbind keypress events
		$(document).unbind("keydown");

		// password input style
		$('#pin').mobilePassword();

		// autofocus input box
		$timeout(function(){
			$('#pinClone0').focus();
		}, 500);

		// on password update
		$('#pinClone0').keydown(function() {

			if($('#pin').val().length > 4){
				$state.go('mainmenu');
			};
		});

	}])

	// main menu controller
	.controller('menuController', [ '$scope', '$state', function($scope, $state) {

		$scope.goToNextPage = function(){

			$state.go('withdraw');
		};

	}])

	// main menu controller
	.controller('withdrawController', [ '$scope', '$state', function($scope, $state) {

		// initiate account slider
		$(".accountslider").Cloud9Carousel( {
			speed: 2,
			bringToFront: true,
			itemClass: 'item',
			farScale: 0.7,
			yRadius: -5,
			buttonLeft: $('#buttonLeft'),
			buttonRight: $('#buttonRight')
		});

		// initate calculator
		$('map area').click(function(){

			if($(this).attr('id') != 'del'){
				if($('.other-amount').val().length < 5 )
					$('.other-amount').val( $('.other-amount').val() +  $(this).attr('id'));
			} else{
				$('.other-amount').val( $('.other-amount').val().substr(0,$('.other-amount').val().length-1));
			};
		});

		// get selected account
		$scope.getSelectedAcc = function(){

			// get index
			var index = $(".accountslider").data("carousel").nearestIndex();
			// set val
			switch(index){
				case 0:
					$scope.data.accName = 'One Account',
					$scope.data.accType = 'Current Account',
					$scope.data.accNo = '955-341-052-3',
					$scope.data.accBal = 55196.35;
					break;
				case 1:
					$scope.data.accName = 'UniPlus',
					$scope.data.accType = 'Saving Account',
					$scope.data.accNo = '900-328-763-5',
					$scope.data.accBal = 18188.88;
					break;
				case 2:
					$scope.data.accName = 'Savings Passbook',
					$scope.data.accType = 'Joint Current Account',
					$scope.data.accNo = '453-032-876-3',
					$scope.data.accBal = 888.88;
					break;
			};

		};

		// quick amount withdrawal
		$scope.quickWithdraw = function(amt){

			$scope.data.withdrawAmt = amt; // withdraw amt
			$scope.getSelectedAcc(); // get account name
			$state.go('summary'); // redirect
		};

		// manual withdrawal
		$scope.manualProceed = function(){

			$scope.quickWithdraw($('.other-amount').val());
		};

		// swipe left
		$scope.carouselSwipeLeft = function(){
			$('#buttonRight').click();
		};

		// swipe right
		$scope.carouselSwipeRight = function(){
			$('#buttonLeft').click();
		};


	}])

	// summaryController controller
	.controller('summaryController', [ '$scope', '$state', function($scope, $state) {

		// display date
		$scope.fullDate = $scope.data.date.getDay() +" "+ $scope.monthNames[$scope.data.date.getMonth()] +" "+$scope.data.date.getFullYear();

	}])

	// acknolegdment controller
	.controller('acknowledgementController', [ '$scope', '$state', '$timeout', function($scope, $state, $timeout) {
		
		// get new time
		$scope.data.date = new Date();
		// date
		$scope.akDate = $scope.data.date.getDay() + " " + $scope.monthNames[$scope.data.date.getMonth()] + " " + $scope.data.date.getFullYear() + " " + $scope.formatAMPM($scope.data.date) + " SGT";

		// redirect
		$timeout(function(){
			$state.go('idle');
		}, 5000)
	}])