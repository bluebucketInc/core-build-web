
'use strict';

//Angular module
angular
	// dependency injection
	.module('uobApp', ['ui.router', 'ngAnimate', 'ngTouch'])

	// Run
	.run(run);

	// 
	run.$inject = ['$rootScope', '$location', '$window', '$state'];
	function run($rootScope, $location, $window, $state) {
	    // on state change success
	    $rootScope.$on('$stateChangeSuccess', function() {
		    document.body.scrollTop = 0;
		    document.documentElement.scrollTop = 0;
		});
	};