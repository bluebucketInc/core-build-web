
'use strict';

//Angular module
angular
	.module('uobApp')

	// Route states
	.config(function($stateProvider, $urlRouterProvider) {

		// otherwise
		$urlRouterProvider.otherwise('/idle');

		$stateProvider

			// idle state
	        .state('idle', {
	            url 		: '/idle',
	            templateUrl : 'app/views/1-idle.html',
	            controller 	: 'idleController as ictrl',
	            language	: true
	        })

			// pin state
	        .state('pin', {
	            url 		: '/pin',
	            templateUrl : 'app/views/2-pin.html',
	            controller 	: 'pinController as pctrl'
	        })

	        // mainMenu state
	        .state('mainmenu', {
	            url 		: '/mainMenu',
	            templateUrl : 'app/views/3-mainMenu.html',
	            controller 	: 'menuController as mctrl'
	        })

	        // withdraw state
	        .state('withdraw', {
	            url 		: '/cashWithdrawal',
	            templateUrl : 'app/views/4-withdraw.html',
	            controller 	: 'withdrawController as wctrl'
	        })

	        // summary state
	        .state('summary', {
	            url 		: '/summary',
	            templateUrl : 'app/views/5-summary.html',
	            controller 	: 'summaryController as sctrl'
	        })

	        // acknowledgement state
	        .state('acknowledgement', {
	            url 		: '/acknowledgement',
	            templateUrl : 'app/views/6-acknowledgement.html',
	            controller 	: 'acknowledgementController as actrl',
	            close		: true
	        })

	});